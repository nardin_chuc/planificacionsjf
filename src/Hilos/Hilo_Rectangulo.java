/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Hilos;

import SJF_Vista.InterfazSJF;
import java.awt.Color;
import static java.awt.Color.BLUE;
import static java.awt.Color.GRAY;
import static java.awt.Color.PINK;
import static java.awt.Color.RED;
import static java.awt.Color.YELLOW;
import java.awt.Graphics;
import java.lang.reflect.Array;

public class Hilo_Rectangulo extends Thread {

    String valor = "";
    int  aux, contador = 0, conta = 0, p1 = 0, p2 = 0, p3 = 0, p4 = 0, lugar = 0;
    public int NumeroMayor = 0;
    int[] Timeproceso = new int[4];
    int[] guia = new int[4];
    int x = 100;
    int y;
    DibujarRectangulo r;
    private HiloTiempo hT;

    private InterfazSJF SJF;

    public Hilo_Rectangulo(InterfazSJF sjf) {
        this.SJF = sjf;
        
    }

    @Override
    public void run() {
       
        if (contador == 3) {

        } else {
            for (int i = 0; i < 4; i++) {
                meterNumero(contador);
                contador++;
                guia[i] = Timeproceso[i];
                NumeroMayor += Timeproceso[i];
                
            }
            
            SJF.jLabel_p1.setText(""+Timeproceso[0]+"s.");
            SJF.jLabel_p2.setText(""+Timeproceso[1]+"s.");
            SJF.jLabel_p3.setText(""+Timeproceso[2]+"s.");
            SJF.jLabel_p4.setText(""+Timeproceso[3]+"s.");
            try {
                Thread.sleep(1200);
            } catch (Exception e) {
                
            }
            hT = new HiloTiempo(SJF);
            hT.setLimite(NumeroMayor);
            hT.start();
            ordenar();
            SJF.btn_Iniciar.setVisible(true);
        }

    }

    public void buscarNumero(int numero) {
        for (int i = 0; i < contador; i++) {
            if (Timeproceso[i] == numero) {                                                    //Validacion si se encuentra en el arreglo
                meterNumero(contador);                                                                    //llama al metodo de meter numero para agregar otro
            } else {

            }
        }
    }

    public void meterNumero(int contador) {   //Metodo Principal para meter numero al arreglo numeros
        aux = (int) Math.floor(Math.random() * 9);                                                     //aux recibira el numero aleatorio
        if (aux == 0) {                                                                                  //Valida si es igual a cero, ya que en las cartas no carta 0
            aux = aux + 1;                                                                               //Si hay un cero, se le sumara un uno, para evitarlo       
        }
        for (int i = contador; i < contador + 1; i++) {
            Timeproceso[i] = aux;
            buscarNumero(aux);
            
        }
    }

    public void dibujar(int limite, int altura, Color color) {
        y = altura;
        for (int i = 0; i < limite; i++) {
            valor = String.valueOf(limite);
        }
        r = new DibujarRectangulo(SJF.JpanelAnimar.getGraphics(), x, y, valor, color, false);

        x = x + 41;
    }

    public void ordenar() {
        MergeSort merge = new MergeSort(Timeproceso);
        merge.sort(Timeproceso, 1, (Timeproceso.length - 1));
        int Mayor = Timeproceso[3];

        for (int i = 0; i < 4; i++) {
                if (Timeproceso[i] == guia[0]) {
                    lugar = i;
                }else if(Timeproceso[i] == guia[1]){
                    lugar = 1;
                }else if(Timeproceso[i] == guia[2]){
                    lugar = 2;
                }else if(Timeproceso[i] == guia[3]){
                    lugar = 3;
                }
                switch (lugar) {

            case 0:
                p1 = Timeproceso[i];
                for (int j = 0; j < p1; j++) {
                    dibujar(p1, 10, RED);
                }
                break;
            case 1:
                p2 = Timeproceso[i];
                for (int k = 0; k < p2; k++) {
                    dibujar(p2, 70, YELLOW);
                }
                break;
            case 2:
                p3 = Timeproceso[i];
                for (int l = 0; l < p3; l++) {
                    dibujar(p3, 130, PINK);
                }
                break;
            case 3:
                p4 = Timeproceso[i];
                for (int m = 0; m < p4; m++) {
                    dibujar(p4, 190, GRAY);
                }
                break;
            default:
                System.out.println("No hay el lugar");
                break;

            }
        }
        
    }
    
    
}

