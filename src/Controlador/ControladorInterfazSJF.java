/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Hilos.HiloTiempo;
import Hilos.Hilo_Rectangulo;
import SJF_Vista.InterfazSJF;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;


public class ControladorInterfazSJF implements ActionListener {
    
    private InterfazSJF SJF;
    private Hilo_Rectangulo hRec;
    int []arreglo = new int [4];
    

    public ControladorInterfazSJF(InterfazSJF sjf){
        this.SJF = sjf;
        this.SJF.btn_Iniciar.addActionListener(this);
        Ver(false);
        hRec = new Hilo_Rectangulo(SJF);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == SJF.btn_Iniciar){
            SJF.btn_Iniciar.setVisible(false);
            SJF.JpanelAnimar.setVisible(true);
            SJF.jPanel_Tiempo.setVisible(true);
            Ver(true);
            Hilo_Rectangulo r1 = new Hilo_Rectangulo(SJF);
            r1.start();
            
            SJF.JpanelAnimar.repaint();
            SJF.jPanel_Tiempo.repaint();
        }
            
    }
    public void Ver(boolean ver){
        SJF.jPanel_Datos.setVisible(ver);
    }
    
}
